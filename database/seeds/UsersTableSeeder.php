<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
        	'name' => 'Ko Ko',
        	'profile' => 'images/user/admin.png',
        	'email' => 'admin@gmail.com',
        	'password' => Hash::make('12345678'),
        	'phone' => '09696571821',
        	'address' => 'Yangon'



        ]);
        $admin->assignRole('admin');

         $customer = User::create([
        	'name' => 'Zaw Htet',
        	'profile' => 'images/user/user.png',
        	'email' => 'user@gmail.com',
        	'password' => Hash::make('12345678'),
        	'phone' => '09696571821',
        	'address' => 'Yangon'



        ]);
         $customer->assignRole('customer');

    }
}
