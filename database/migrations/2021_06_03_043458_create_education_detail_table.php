<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('job_seeker_id');
            $table->foreign('job_seeker_id')->references('id')->on('job_seeker_profile');
            $table->string('degree_name')->nullable();
            $table->string('institute_name')->nullable();
            $table->date('starting_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_detail');
    }
}
