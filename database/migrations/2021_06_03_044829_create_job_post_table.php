<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('job_post_id');
            $table->foreign('job_post_id')->references('id')->on('job_post');
            $table->unsignedInteger('job_type_id');
            $table->foreign('job_type_id')->references('id')->on('job_type');
            $table->unsignedInteger('job_post_skill_set_id');
            $table->foreign('job_post_skill_set_id')->references('id')->on('job_post_skill_set');
            $table->unsignedInteger('region_id');
            $table->foreign('region_id')->references('id')->on('region');
            $table->string('job_post_name');
            $table->string('job_post_description');
            $table->string('company_name');
            $table->string('company_description');
            $table->string('company_address');
            $table->string('company_email');
            $table->string('company_ph');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post');
    }
}
