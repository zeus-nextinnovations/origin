<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostSkillSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_skill_set', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('program_language_id');
            $table->foreign('program_language_id')->references('id')->on('program_language');
            $table->unsignedInteger('framework_id');
            $table->foreign('framework_id')->references('id')->on('framework');
            $table->unsignedInteger('job_role_id');
            $table->foreign('job_role_id')->references('id')->on('job_role');
            $table->date('period_from');
            $table->date('period_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_skill_set');
    }
}
