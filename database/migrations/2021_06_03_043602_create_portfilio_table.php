<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfilioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfilio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('job_seeker_id');
            $table->foreign('job_seeker_id')->references('id')->on('job_seeker_profile');
            $table->unsignedInteger('prj_type_id');
            $table->foreign('prj_type_id')->references('id')->on('prj_type');
            $table->string('prj_name');
            $table->string('prj_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfilio');
    }
}
