
<!DOCTYPE html>
<html>
<head>
	<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}" type="text/css">

     <!-- iconfont CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('icon/icofont/icofont.min.css')}}">
	<title></title>
</head>
<body>
	<img src="{{asset('images/errors/404.jpg')}}"><br>
	<button class="btn btn-danger">Go Back to Home
		<i class="icofont-hand-right"></i>
	</button>
</body>
</html>



