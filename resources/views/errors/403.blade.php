
<!DOCTYPE html>
<html>
<head>
	<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}" type="text/css">

     <!-- iconfont CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('icon/icofont/icofont.min.css')}}">
	<title></title>
</head>
<body>

	<div class="col-sm" style="max-width: 50rem; 
							margin: 0 auto;
							float: none;
							margin-bottom: 10px;">
	<img src="{{asset('images/errors/403.png')}}" class="img-fluid" {{-- style="width: 600px; height: 500px;" --}}>
	</div><br>


	<div style="max-width: 18rem; 
				margin: 0 auto;
				float: none;
				margin-bottom: 10px;">

		
	<a href="{{route('index')}}" type="button" class="btn btn-danger">
		<i class="icofont-hand-right"></i>
	Go back to Home</a>
	</div>
</body>
</html>



