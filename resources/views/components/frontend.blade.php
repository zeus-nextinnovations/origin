<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="{{csrf_token()}}" />
        <title>Next Innovation</title>
        <link rel="stylesheet" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
        <link rel="stylesheet" media="screen" href="{{asset('frontend/css/style.css')}}" />
        <link rel="stylesheet" media="screen" href="{{asset('frontend/css/uikit.min.css')}}" />
        <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
        
    </head>
    <body>
        <header id="header" class="site-header" data-uk-sticky="top: 300; animation: uk-animation-slide-top">
            <div class="uk-container uk-container-expand">
                <div class="uk-flex uk-flex-between uk-flex-middle">
                    <h1 class="site-logo">
                        <a href="index.html"><img src="{{asset('/frontend/img/TalentCloud.png')}}"></a>
                    </h1>
                    <nav class="primary-menu uk-visible@l">
                        <ul class="menu">
                            <li><a href="{{route('login')}}">Login</a></li>
                            <li><a href="{{route('register')}}">Register</a></li>
                        </ul>
                    </nav>
                    
                    <div class="mobile-menu uk-hidden@s">
                        
                        <div class="trigger" data-uk-toggle="target: #mobile-menu">
                            <div class="toggle">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                .site-logo a{
                    color:#fff;
                }
                .menu a{
                    padding: 10px 20px;
                    border: 1px solid #000;
                    background: #000;
                    color:#fff;
                    border-radius: 5px;
                }
                .menu li:last-child a, .mobile-nav li:last-child a{
                    background: #ffa500;
                    border: 1px solid #ffa500;
                    color:#000;
                }
                #mobile-menu .inner .content .mobile-nav > li > a{
                    padding: 10px 20px;
                    border: 1px solid #000;
                    background: #000;
                    color:#fff;
                    border-radius: 5px;
                    margin:10px;
                    text-align:center;
                }
                #mobile-menu .inner .content .mobile-nav > li:last-child > a{
                    padding: 10px 20px;
                    border: 1px solid #ffa500;
                    background: #ffa500;
                    color:#fff;
                    border-radius: 5px;
                    margin:10px;
                    text-align:center;
                }
            </style>
        </header>

   {{$slot}}

   <footer id="footer" class="site-footer">
            <div class="copyrights">
                <div class="uk-container">
                    2021 © All rights reserved.
                </div>
            </div>
        </footer>
        <script src="{{asset('frontend/js/uikit.min.js')}}"></script>
        <script src="{{asset('frontend/js/main.js')}}"></script>

    </body>
</html>