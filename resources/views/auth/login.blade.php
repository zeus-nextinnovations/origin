<x-frontend>

           <!-- Login Form -->
        <div class="container-fluid" id="grad1">
            <div class="row justify-content-center mt-0">
                <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <h2><strong>Login Form</strong></h2>
                        <div class="row">
                            <div class="col-md-12 mx-0">
                                <form method="POST" action="{{ route('login') }}" id="msform">
                                @csrf
                                    <fieldset>
                                        <div class="form-card">

                                        <div class="form-group">
                                            <input type="email" name="email" placeholder="Email Address" value="{{ old('email') }}"/> 
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <input type="password" name="password" placeholder="Password" /> 
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                            <div class="custom-control custom-checkbox mb-3"> 
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"> 
                                                <label class="custom-control-label" for="customCheck1">Remember password?</label> 
                                            </div> 

                                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
                                        </div> 
                                        <div class="form-card">
                                            <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button> <br>
                                            <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button>
                                        </div>
                                    </fieldset>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</x-frontend>