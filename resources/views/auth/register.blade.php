<x-frontend>


                    <!-- MultiStep Form -->
        <div class="container-fluid" id="grad1">
            <div class="row justify-content-center mt-0">
                <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <h2><strong>Registeration Form</strong></h2>
                        <p>Fill all form field to go to next step</p>
                        <div class="row">
                            <div class="col-md-12 mx-0">
                                <form id="msform">
                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li class="active" id="account"><strong>Account</strong></li>
                                        <li id="personal"><strong>Personal Information</strong></li>
                                        <li id="skill"><strong>Skills</strong></li>
                                        <li id="qua"><strong>Qualification</strong></li>
                                        <li id="confirm"><strong>Finish</strong></li>
                                    </ul> <!-- fieldsets -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Account Information</h2> <input type="email" name="email" placeholder="Email Id" /> <input type="text" name="name" placeholder="UserName" /> <input type="password" name="pwd" placeholder="Password" /> <input type="password" name="cpwd" placeholder="Confirm Password" />
                                        </div> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Personal Information</h2> <input type="text" name="firstname" placeholder="First-Name" /> <input type="text" name="lastname" placeholder="Last-Name" /> <label for="birthday">Date of Birth</label> <input type="date" name="dateofbirth"/>
                                            
                                            <div class="gender">
                                            <!--  started Gender -->
                                            <h3 class="fs-title">Gender</h3>
                                            <input type="radio" id="male" name="gender" value="male">
                                            <label for="male">Male</label><br>
                                            <input type="radio" id="female" name="gender" value="female">
                                            <label for="female">Female</label><br>
                                            <input type="radio" id="other" name="gender" value="other">
                                            <label for="other">Other</label>
                                            <!-- end of gender -->
                                            </div>

                                        <input type="text" name="address" placeholder="Address" />
                                        <input type="text" name="nationality" placeholder="Nationality"/>

                                        <!--  started Salary -->
                                        <input type="number" name="currentsalary" placeholder="Current Salary" />
                                        <input type="number" name="expectedsalary" placeholder="Expected Salary" />
                                        <label for="cars">Choose Currency:</label>
                                        <select name="cars" id="cars">
                                            <option value="volvo">Ks</option>
                                            <option value="saab">Yan</option>
                                            <option value="opel">Rupy</option>
                                            <option value="audi">Bet</option>
                                        </select>
                                        <!-- ended of salary -->
                                        

                                            <input type="text" name="phno" placeholder="Contact No." /> <input type="text" name="nrc" placeholder="NRC Number" />  <input type="textarea" name="self" placeholder="Self Introduction" />
                                        </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-6" style="padding-right:0px;">
                                                <div class="form-card">
                                                    <h2 class="fs-title">Skills</h2>
                                                    <div class="skill-group">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="html" value="html">HTML
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="css" value="css">CSS
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="javascript" value="javascript">Javascript
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="php" value="php">PHP
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="python" value="python">Python
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="java" value="java">Java
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="padding-left:0px;">
                                                <div class="form-card">
                                                    <p>Please choose the framework you have experienced!</p>
                                                    <div class="php-framework" style="display:none;">

                                                        <select id="php-frame" name="framework[]" multiple class="form-control" >
                                                            <option value="Codeigniter">Codeigniter</option>
                                                            <option value="CakePHP">CakePHP</option>
                                                            <option value="Laravel">Laravel</option>
                                                            <option value="YII">YII</option>
                                                            <option value="Zend">Zend</option>
                                                            <option value="Symfony">Symfony</option>
                                                            <option value="Phalcon">Phalcon</option>
                                                            <option value="Slim">Slim</option>
                                                        </select>
                                                       
                                                    </div>

                                                    <div class="python-framework" style="display:none;">
                                                        <select id="python-frame" name="framework[]" multiple class="form-control" >
                                                            <option value="Django">Django</option>
                                                            <option value="Pyramid">Pyramid</option>
                                                            <option value="TurboGears">TurboGears</option>
                                                            <option value="Web2py">Web2py</option>
                                                            <option value="Flask">Flask</option>
                                                        </select>
                                                            
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                        
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Qualification</h2> 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="degreename" placeholder="Degree Name" /> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="date" name="degreedate" placeholder="Degree Date" /> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="institutename" placeholder="Institute Name" /> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="date" name="institutedate" placeholder="Institute Date" /> 
                                                </div>
                                            </div>
                                            
                                            
                                        </div> 
                                        <div class="form-card">
                                            <h2 class="fs-title">Business Experience</h2> 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="exp-title" placeholder="Experience Title" /> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="company" placeholder="Company Name" /> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="position" placeholder="Position" /> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="startDate">Start Date</label>
                                                    <input id="startDate" name="startDate" type="text" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="endDate">End Date</label>
                                                    <input id="endDate" name="endDate" type="text" class="form-control" />                                               </div>
                                            </div>
  
                                        </div> 
                                        <div class="form-card">

                                            <h2 class="fs-title">Portfolio</h2> 
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="projectname" placeholder="Project Name" /> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="projectdescription" placeholder="Project Description" /> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="projecttypename" placeholder="Project Type Name" /> 
                                                </div>                      
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="projectlink" placeholder="Project Link" /> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="file" name="portfolio"/> 
                                                </div>
                                            </div>

                                        </div> 
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title text-center">Success !</h2> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                            </div> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-7 text-center">
                                                    <h5>You Have Successfully Register</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
</x-frontend>