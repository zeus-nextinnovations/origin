
$(document).ready(function(){

cartNoti();
showTable();


	$('#shoppingcart_table').on('click','.plus_btn',function(){

		var id = $(this).data('id');
		var itemString = localStorage.getItem('cart');
		var itemArray = JSON.parse(itemString);
		$.each(itemArray,function(i,v){
			console.log(id)
			if(i==id){
				v.qty++;
			}
		})

		cart = JSON.stringify(itemArray);
		localStorage.setItem('cart',cart);
		showTable();
		cartNoti();
	});

	$('#shoppingcart_table').on('click','.minus_btn',function(){
		/*alert("hi");*/

		var id = $(this).data('id');
		var itemString = localStorage.getItem('cart');
		var itemArray = JSON.parse(itemString);
		$.each(itemArray,function(i,v){
			console.log(id)
			if(i==id){
				v.qty--;
				if (v.qty==0) {
					itemArray.splice(id,1);
				}
			}
		})

		cart = JSON.stringify(itemArray);
		localStorage.setItem('cart',cart);
		showTable();
		cartNoti();
	});

	$('#shoppingcart_table').on('click','.remove',function(){
		/*alert("hi");*/

		var id = $(this).data('id');
		var itemString = localStorage.getItem('cart');
		var itemArray = JSON.parse(itemString);
		$.each(itemArray,function(i,v){
			console.log(id)
			if(i==id){
				
					itemArray.splice(id,1);
				}
			
		})

		cart = JSON.stringify(itemArray);
		localStorage.setItem('cart',cart);
		showTable();
		cartNoti();
	});

$('.addtocartBtn').on('click',function(){

		var id = $(this).data('id');
		var name = $(this).data('name');
		var codeno = $(this).data('codeno');
		var photo= $(this).data('photo');
		var unitprice = $(this).data('unitprice');
		var discount= $(this).data('discount');
		//var discount = unitprice-(unitprice*(discount_percent/100));
		var qty = 1;


		var mylist = {
			id:id, codeno:codeno,name:name, photo:photo, unitprice:unitprice, 
			discount:discount, qty:qty
		};
		console.log(mylist);

		var cart = localStorage.getItem('cart');
			var cartArray;
			
			if (cart==null){
				cartArray = Array();
			}else{
				cartArray = JSON.parse(cart);
			}	

			var status=false;

			$.each(cartArray, function(i,v){
				if (id == v.id) {
					v.qty++;
					status = true;
				}
			});

			if(!status) {
				cartArray.push(mylist);
			}

			var cartData = JSON.stringify(cartArray);
			localStorage.setItem("cart",cartData);


			cartNoti();



})

function cartNoti(){
		var cart = localStorage.getItem('cart')
		if(cart){
			var cartArray = JSON.parse(cart);
			var total=0;
			var noti=0;
			$.each(cartArray, function(i,v){
				var unitprice = v.unitprice;
				var discount = v.discount;
				var qty = v.qty;
				if (discount){
					var unitprice = discount;
				}else{
					var unitprice = unitprice;
				}
				var subtotal = unitprice * qty;

				noti += qty ++;
				total += subtotal ++;

			})

			$('.cartNoti').html(noti);
			$('.cartTotal').html(total+'Ks');
		}else{
			$('.cartNoti').html('0');
			$('.cartTotal').html('0'+'Ks');
		}
	}

	function showTable(){
		var cart = localStorage.getItem('cart');

		if(cart) {
			$('.shoping-cart spad').show();
			$('.noneshoppingcart_div').hide();

			var cartArray = JSON.parse(cart);
			var shoppingcartData='';


			if (cartArray.length > 0) {
				var total=0;
				$.each(cartArray, function(i,v){
					var id = v.id;
					var codeno = v.codeno;
					var name = v.name;
					var unitprice = v.unitprice;
					var discount = v.discount;
					var photo = v.photo;
					var qty = v.qty;

					if(discount) {
						var unitunitprice = discount;
					}else{
						var unitunitprice = unitprice;
					}
					var subtotal = unitunitprice *qty;

					shoppingcartData += `<tr>
                                    <td class="shoping__cart__item">
                                        <img src="${photo}" style="width:100px; height:100px;">
                                        <h5>${name}</h5>
                                        <p> ${codeno} </p>
                                    </td>
                                    <td class="shoping__cart__unitprice">`;

										if (discount){ 
											shoppingcartData +=`<p class="text-danger">
											${discount}Ks</p>
											<p class="font-weight-bold">
											<del> ${unitprice}Ks</del>`}
											else{

												shoppingcartData +=`<p class="font-weight-bold">
												${unitprice}Ks
												</p>`;
											}


											shoppingcartData +=	`
                                    </td>
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div>
                                                <button class="btn btn-light plus_btn" data-id=${i}> 
												<i class="icofont-plus"></i> 
												</button>
												
												</button>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div>
                                                <p> ${qty} </p>
                                            </div>
                                        </div>
                                    </td>


                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div>
                                               <button class="btn btn-light minus_btn" data-id=${i}> 
												<i class="icofont-minus"></i>
												</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        <p>${subtotal}</p>
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <button class="btn btn-light remove btn-sm" data-id=${i}
										style="border-radius: 50%"> 
										<i class="icofont-close-line"></i> 
										</button>
                                    </td>
                                </tr>`;

						total += subtotal ++;
						$('total').html(total+"Ks");
					});


				$('#shoppingcart_table').html(shoppingcartData);


			}else{
				$('.shoppingcart_div').hide();
				$('.noneshoppingcart_div').show();
				$('#shoppingcart_table').hide();
			}
		}else{
			$('.shoppingcart_div').hide();
			$('.noneshoppingcart_div').show();
		 }
	}

	$('.checkoutBtn').click(function(){
			var cart=localStorage.getItem("cart");
			var note= $('#notes').val();

			$.ajaxSetup({headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')}});

			$.post('/order',{data:cart,note:note},function(response){

				localStorage.clear();
				location.href="ordersuccess";

			});


			console.log(cart);
			console.log(note);
})

	



});