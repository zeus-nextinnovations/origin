jQuery(document).ready(function ($) {
    "use strict";

    // Preloader
    // ------------------------------------------------------
    $(window).on('load', function () {
        setTimeout(function () {
            $("#loader").fadeOut(200);
        }, 200);
    });

    // Main Sliders
    // ------------------------------------------------------
    var $this = $('.site-slider'),
        slider = $this.find('.owl-carousel');

    if ($this.hasClass('modern-layout')) {
        slider.owlCarousel({
            items: 1,
            dots: false,
            navText: ['Prev', 'Next'],
            navContainer: '#slider-nav'
        });
    } else if ($this.hasClass('two-cols')) {
        slider.owlCarousel({
            items: 2,
            dots: false,
            margin: 4
        });
    } else if ($this.hasClass('three-cols')) {
        slider.owlCarousel({
            items: 3,
            dots: false,
            margin: 4
        });
    } else if ($this.hasClass('four-cols')) {
        slider.owlCarousel({
            items: 4,
            dots: false,
            margin: 4
        });
    }
    
    slider.owlCarousel({
        items: 1,
        autoplay: false,
        autoplayHoverPause: true,
        autoHeight: true,
        loop: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        margin: 0,
        dots: true,
        smartSpeed: 450,
        navText: ['Prev', 'Next'],
        navContainer: '#slider-nav',
        dotsContainer: '#slider-dots'
    });

    // Custom Main Slider Navigation
    $('.next').on('click', function () {
        slider.trigger('.owl-next');
    })
    $('.prev').on('click', function () {
        slider.trigger('.owl-prev');
    })

    // Trending Carousel Slider
    $('.site-trending .owl-carousel').owlCarousel({
        items: 5,
        margin: 30,
        autoplay: true,
        autoplayHoverPause: true,
        dots: false,
        smartSpeed: 450,
        dots: true,
        dotsContainer: '#trending-dots',
        responsive: {
            0: {
                items: 1,
                margin: 10,
            },
            640: {
                items: 2,
                margin: 40,
            },
            960: {
                items: 3,
                margin: 40,
            },
            1200: {
                items: 4,
                margin: 40,
            },
            1600: {
                items: 5,
                margin: 10,
            }
        }
    });

});

$(document).ready(function(){

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".next").click(function(){

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;

    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    next_fs.css({'opacity': opacity});
    },
    duration: 600
    });
    });

    $(".previous").click(function(){

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;

    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    previous_fs.css({'opacity': opacity});
    },
    duration: 600
    });
    });

    $('.radio-group .radio').click(function(){
    $(this).parent().find('.radio').removeClass('selected');
    $(this).addClass('selected');
    });

    $(".submit").click(function(){
    return false;
    })

    });
   
    var bindDateRangeValidation = function (f, s, e) {
if(!(f instanceof jQuery)){
    console.log("Not passing a jQuery object");
}

var jqForm = f,
startDateId = s,
endDateId = e;

var checkDateRange = function (startDate, endDate) {
var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
return isValid;
}

var bindValidator = function () {
var bstpValidate = jqForm.data('bootstrapValidator');
var validateFields = {
    startDate: {
        validators: {
            notEmpty: { message: 'This field is required.' },
            callback: {
                message: 'Start Date must less than or equal to End Date.',
                callback: function (startDate, validator, $field) {
                    return checkDateRange(startDate, $('#' + endDateId).val())
                }
            }
        }
    },
    endDate: {
        validators: {
            notEmpty: { message: 'This field is required.' },
            callback: {
                message: 'End Date must greater than or equal to Start Date.',
                callback: function (endDate, validator, $field) {
                    return checkDateRange($('#' + startDateId).val(), endDate);
                }
            }
        }
    },
      customize: {
        validators: {
            customize: { message: 'customize.' }
        }
    }
}
if (!bstpValidate) {
    jqForm.bootstrapValidator({
        excluded: [':disabled'], 
    })
}

jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);

};

var hookValidatorEvt = function () {
var dateBlur = function (e, bundleDateId, action) {
    jqForm.bootstrapValidator('revalidateField', e.target.id);
}

$('#' + startDateId).on("dp.change dp.update blur", function (e) {
    $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
    dateBlur(e, endDateId);
});

$('#' + endDateId).on("dp.change dp.update blur", function (e) {
    $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
    dateBlur(e, startDateId);
});
}

bindValidator();
hookValidatorEvt();
};


$(function () {
var sd = new Date(), ed = new Date();

$('#startDate').datetimepicker({ 
pickTime: false, 
format: "YYYY/MM/DD", 
defaultDate: sd, 
maxDate: ed 
});

$('#endDate').datetimepicker({ 
pickTime: false, 
format: "YYYY/MM/DD", 
defaultDate: ed, 
minDate: sd 
});

//passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
bindDateRangeValidation($("#form"), 'startDate', 'endDate');
});

// Form Js   
$(document).ready(function(){

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    $(".next").click(function(){

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;

    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    next_fs.css({'opacity': opacity});
    },
    duration: 600
    });
    });

    $(".previous").click(function(){

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;

    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    previous_fs.css({'opacity': opacity});
    },
    duration: 600
    });
    });

    $('.radio-group .radio').click(function(){
    $(this).parent().find('.radio').removeClass('selected');
    $(this).addClass('selected');
    });

    $(".submit").click(function(){
    return false;
    })

    });
   
    var bindDateRangeValidation = function (f, s, e) {
if(!(f instanceof jQuery)){
    console.log("Not passing a jQuery object");
}

var jqForm = f,
startDateId = s,
endDateId = e;

var checkDateRange = function (startDate, endDate) {
var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
return isValid;
}

var bindValidator = function () {
var bstpValidate = jqForm.data('bootstrapValidator');
var validateFields = {
    startDate: {
        validators: {
            notEmpty: { message: 'This field is required.' },
            callback: {
                message: 'Start Date must less than or equal to End Date.',
                callback: function (startDate, validator, $field) {
                    return checkDateRange(startDate, $('#' + endDateId).val())
                }
            }
        }
    },
    endDate: {
        validators: {
            notEmpty: { message: 'This field is required.' },
            callback: {
                message: 'End Date must greater than or equal to Start Date.',
                callback: function (endDate, validator, $field) {
                    return checkDateRange($('#' + startDateId).val(), endDate);
                }
            }
        }
    },
      customize: {
        validators: {
            customize: { message: 'customize.' }
        }
    }
}
if (!bstpValidate) {
    jqForm.bootstrapValidator({
        excluded: [':disabled'], 
    })
}

jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);

};

var hookValidatorEvt = function () {
var dateBlur = function (e, bundleDateId, action) {
    jqForm.bootstrapValidator('revalidateField', e.target.id);
}

$('#' + startDateId).on("dp.change dp.update blur", function (e) {
    $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
    dateBlur(e, endDateId);
});

$('#' + endDateId).on("dp.change dp.update blur", function (e) {
    $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
    dateBlur(e, startDateId);
});
}

bindValidator();
hookValidatorEvt();
};


$(function () {
var sd = new Date(), ed = new Date();

$('#startDate').datetimepicker({ 
pickTime: false, 
format: "YYYY/MM/DD", 
defaultDate: sd, 
maxDate: ed 
});

$('#endDate').datetimepicker({ 
pickTime: false, 
format: "YYYY/MM/DD", 
defaultDate: ed, 
minDate: sd 
});

//passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
bindDateRangeValidation($("#form"), 'startDate', 'endDate');
});
