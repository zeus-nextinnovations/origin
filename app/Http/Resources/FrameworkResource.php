<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FrameworkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'framework_id'  => $this->id,
            'framework_name' => $this->name,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')
        ];
    }
}
