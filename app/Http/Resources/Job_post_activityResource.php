<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Job_post_activityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'job_post_activity_id'  => $this->id,
            'user_id' => $this->id,
            'user' => new UserResource(User::find($this->user_id)),
            'apply_date'  => $this->applydate,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')

        ];
    }
}
