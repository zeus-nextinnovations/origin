<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PortfilioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'portfolio_id'=> $this->id,
            'job_seeker_profile_id' => $this->jobseekerprofile_id,
            'job_seeker_profile' => new Job_seeker_profileResource(Job_seeker_profileResource::find($this->job_seeker_profile_id)),
            'prj_type_id' => $this->prj_type_id,
            'prj_type' => new Prj_typeResource(Prj_typeResource::find($this->prj_type_id)),
            'prj_name' => $this->name,
            'prj_description' => $this->prjdescription,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')

        ];
    }
}
