<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Role_userResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'role_user_id'  => $this->id,
            'user_id' => $this->id,
            'user' => new UserResource(User::find($this->user_id)),
            'role_id' => $this->id,
            'role' => new RoleResource(Role::find($this->role_id)),
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')
        ];
    }
}
