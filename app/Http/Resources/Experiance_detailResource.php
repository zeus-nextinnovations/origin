<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Experiance_detailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'experiance_detail_id'=> $this->id,
            'job_seeker_profile_id' => $this->jobseekerprofile_id,
            'job_seeker_profile' => new Job_seeker_profileResource(Job_seeker_profileResource::find($this->job_seeker_profile_id)),
            'company_name' => $this->name,
            'position' => $this->position,
            'resposibility' => $this->resposibility,
            'starting_date' => $this->startingdate,
            'finished_date' => $this->finishdate,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')
            
        ];
    }
}
