<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Job_seeker_profileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $baseurl = URL('/');

        return[
            'job_seeker_profile_id'  => $this->id,
            'user_id' => $this->id,
            'user' => new UserResource(User::find($this->user_id)),
            'first_name' => $this->firstname,
            'last_name' => $this->lastname,
            'date_of_birth' => $this->dateofbirth,
            'gender' => $this->gender,
            'ph_no' => $this->phno,
            'address' => $this->address,
            'nationality' => $this->nationality,
            'current_salary' => $this->currentsalary,
            'expected_salary' => $this->expectedsalary,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')
        ];
    }
}
