<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Job_roleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'job_role_id'  => $this->id,
            'job_role_name' => $this->jobrolename,
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')
        ];
    }
}
