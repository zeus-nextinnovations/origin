<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Job_seeker_skill_setResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'job_seeker_skill_set_id' => $this->id,
            'program_language_id' => $this->program_language_id,
            'programlangage' => new Program_languageResource(Program_language::find($this->program_langage_id)),
            'framework_id' => $this->framework_id,
            'framework' => new FrameworkResource(Framework::find($this->framework_id)),
            'job_role_id' => $this->job_role_id,
            'jobrole' => new Job_roleResource(Job_role::find($this->job_role_id)),
            'created_at' => $baseurl.'/'.$this->created_at->format('d-m-Y'),
            'updated_at' => $baseurl.'/'.$this->updated_at->format('d-m-Y')

        ];
    }
}
